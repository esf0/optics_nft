#!/bin/bash

cd build
source /opt/intel/mkl/bin/mklvars.sh intel64

g++ -c -o nftanalyse.o ../src/nftanalyse.cpp -I../include -I../lib/fftw++/ -lpthread -fopenmp
g++ -c -o signal.o ../src/signal.cpp -I../include -I../lib/fftw++/ -lpthread -fopenmp
g++ -c -o ofdmsignal.o ../src/ofdmsignal.cpp -I../include -I../lib/fftw++/ -lpthread -fopenmp
g++ -c -o wdmsignal.o ../src/wdmsignal.cpp -I../include -I../lib/fftw++/ -lpthread -fopenmp
g++ -c -o satsumayajimasignal.o ../src/satsumayajimasignal.cpp -I../include -I../lib/fftw++/ -lpthread -fopenmp
ar ru libnft.a *.o
cp libnft.a ../lib/.
rm *.o
cd ..
