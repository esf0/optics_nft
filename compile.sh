#!/bin/bash

source /opt/intel/composerxe/bin/compilervars.sh intel64

cd build
g++ -std=c++11 -c -o nftanalyse.o ../src/nftanalyse.cpp -I../include -I../lib/fftw++/ -fopenmp -lpthread
g++ -std=c++11 -c -o signal.o ../src/signal.cpp -I../include -I../lib/fftw++/ -fopenmp -lpthread
g++ -std=c++11 -c -o ofdmsignal.o ../src/ofdmsignal.cpp -I../include -I../lib/fftw++/ -fopenmp -lpthread
g++ -std=c++11 -c -o wdmsignal.o ../src/wdmsignal.cpp -I../include -I../lib/fftw++/ -fopenmp -lpthread
g++ -std=c++11 -c -o satsumayajimasignal.o ../src/satsumayajimasignal.cpp -I../include -I../lib/fftw++/ -fopenmp -lpthread
ar ru libnft.a *o
cp libnft.a ../lib/
cd ..

g++ -std=c++11 -Wall -m64 -o bin/read_results_write src/read_results_write.cpp -Llib/ -Iinclude/ -lnft -Ilib/fftw++ -lfftw++ -I${MKLROOT}/include -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_intel_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -L"${MKLROOT}/../compiler/lib/intel64" -liomp5 -lpthread -ldl -lm -fopenmp -lfftw3 -lfftw3_omp -O3

