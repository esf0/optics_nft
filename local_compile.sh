#!/bin/bash

cd build
source /opt/intel/mkl/bin/mklvars.sh intel64

g++ -c -o nftanalyse.o ../src/nftanalyse.cpp -I../include -I../lib/fftw++/ -lpthread -fopenmp -DOMP_NUM_THREADS=4
g++ -c -o signal.o ../src/signal.cpp -I../include -I../lib/fftw++/ -lpthread -fopenmp -DOMP_NUM_THREADS=4
g++ -c -o ofdmsignal.o ../src/ofdmsignal.cpp -I../include -I../lib/fftw++/ -lpthread -fopenmp -DOMP_NUM_THREADS=4
g++ -c -o wdmsignal.o ../src/wdmsignal.cpp -I../include -I../lib/fftw++/ -lpthread -fopenmp -DOMP_NUM_THREADS=4
g++ -c -o satsumayajimasignal.o ../src/satsumayajimasignal.cpp -I../include -I../lib/fftw++/ -lpthread -fopenmp -DOMP_NUM_THREADS=4
ar ru libnft.a *.o
cp libnft.a ../lib/.
rm *.o
cd ..

g++ -std=c++11 -Wall -m64 -o $1 $2 -Llib/ -Iinclude/ -lnft -Ilib/fftw++ -lfftw++ -I${MKLROOT}/include -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_intel_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -L"${MKLROOT}/../compiler/lib/intel64" -liomp5 -lpthread -ldl -lm -fopenmp -lfftw3 -lfftw3_omp -DOMP_NUM_THREADS=4
