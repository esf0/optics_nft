// This file contains realization of class SatsumaYajimaSignal
// author: Egor Sedov
// date: 24.07.2018
// e-mail: egor.sedoff@gmail.com

#include "satsumayajimasignal.h"

SatsumaYajimaSignal::SatsumaYajimaSignal() : Signal() {

  this->setSignalType("satsumayajima");
  ampl = 1;
  int nPoints = 128;
  std::vector<std::complex<double>> data(nPoints);
  std::vector<std::complex<double>> signal(nPoints);
  double timeBoxSize = this->getTimeBoxSize();
  double dt = timeBoxSize/nPoints;

  for ( int i = 0; i < nPoints; i++ ) {
    data[i] = timeBoxSize/2 + i*dt;
    signal[i] = ampl/cosh( -timeBoxSize/2 + i*dt );
  }
  this->setData(data);
  this->setSignal(signal);

}

SatsumaYajimaSignal::SatsumaYajimaSignal( double ampl, double timeBoxSize, int nPoints ) : Signal() {

  this->setSignalType("satsumayajima");
  this->ampl = ampl;
  std::vector<std::complex<double>> data(nPoints);
  std::vector<std::complex<double>> signal(nPoints);
  this->setTimeBoxSize(timeBoxSize);
  double dt = timeBoxSize/nPoints;

  for ( int i = 0; i < nPoints; i++ ) {
    data[i] = timeBoxSize/2 + i*dt;
    signal[i] = ampl/cosh( -timeBoxSize/2 + i*dt );
  }
  this->setData(data);
  this->setSignal(signal);

}

void SatsumaYajimaSignal::printParameters() {
  Signal::printParameters();
  std::cout << "\n***** SatsumaYajima PARAMETERS *****\n" << std::endl
    << "Amplitude : " << ampl << std::endl;
}

int SatsumaYajimaSignal::printToFile(std::string file) {
  std::ofstream outFile;
  outFile.open(file);
  outFile << this->getSignalType() << std::endl
    << this->getNormType() << std::endl
    << this->getNormL1() << std::endl
    << this->getNormL2() << std::endl
    << this->getPeakToAverage() << std::endl
    << this->getTimeBoxSize() << std::endl
    << ampl << std::endl
    << this->getNData() << std::endl;

  std::vector<std::complex<double>> data = getData();
  std::vector<std::complex<double>> signal = getSignal();
  for ( int i = 0; i < this->getNData(); i++ ) {
    outFile << data[i].real() << " " << data[i].imag() << std::endl;
  }
  outFile << this->getNSignal() << std::endl;
  for ( int i = 0; i < this->getNSignal(); i++ ) {
    outFile << signal[i].real() << " " << signal[i].imag() << std::endl;
  }
  outFile.close();

  return 0;
}
