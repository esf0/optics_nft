// This file define NFT analyse methods
// author: Egor Sedov
// date: 29.06.2018
// e-mail: egor.sedoff@gmail.com

#ifndef NFTANALYSE_CPP
#define NFTANALYSE_CPP

#include "nftanalyse.h"

std::vector<std::complex<double>> gridXi(int n, int* N, double* L) {
  int totalN = 0;
  double totalL = 0;
  for( int i = 0; i < n; i++ ) {
    totalN += N[i];
    totalL += L[i];
  }
  std::vector<std::complex<double>> xi(totalN);

  int currentN = 0;
  double currentL = -totalL/2.;
  for( int i = 0; i < n; i++ ) {
    double dl = L[i]/N[i];
    for( int j = 0; j < N[i]; j++ ) {
      xi[currentN + j] = currentL + dl*j;
    }
    currentN += N[i];
    currentL += L[i];
  }

  return xi;
}

std::vector<std::complex<double>>* getRectBoxXi(double boxSize, int N, std::complex<double> startPoint) {
  double step = boxSize / (double)(N - 1);
  // std::vector<std::complex<double>> xi(4*N - 4);
  std::vector<std::complex<double>>* xi = new std::vector<std::complex<double>>();

  for( int i = 0; i < 4*N - 3; i++ ) {
    std::complex<double> point;
    if( i < N ) {
      std::complex<double> tempPoint(startPoint.real() + step*i, startPoint.imag());
      point = tempPoint;
    }
    if( i >= N && i < 2*N - 1 ) {
      std::complex<double> tempPoint(startPoint.real() + boxSize, startPoint.imag() + step*(i - N + 1));
      point = tempPoint;
    }
    if( i >= 2*N - 1 && i < 3*N - 2 ) {
      std::complex<double> tempPoint(startPoint.real() + boxSize - step*(i - 2*N + 2), startPoint.imag() + boxSize);
      point = tempPoint;
    }
    if( i >= 3*N - 2 && i < 4*N - 4 ) {
      std::complex<double> tempPoint(startPoint.real(), startPoint.imag() + boxSize - step*(i - 3*N + 3));
      point = tempPoint;
    }

    xi->push_back(point);
  }

  return xi;
}

std::vector<std::complex<double>>* getHexBoxXi(double boxSize, int N, std::complex<double> startPoint) {

}

std::complex<double> findZeroBox(std::string boxType, solitonMap cell, std::vector<std::complex<double>> (*f)(std::complex<double>)) {

  std::vector<std::complex<double>> result;
  if( boxType == "rect" ) {

    const int n = 12;
    solitonMap subCell;

    for( int i = 0; i < n; i++ ) {
      for( int j = 0; j < n; j++ ) {
        double overcover = 1.01; // Add 1% to subsell size to cover borders
        if( i == n - 1 || j == n - 1 )
          overcover = 1.0; // If it is border subcell we do not need to cover
        subCell.boxSize = cell.boxSize / n * overcover;
        subCell.startPoint.real(cell.startPoint.real() + cell.boxSize / n * j);
        subCell.startPoint.imag(cell.startPoint.imag() + cell.boxSize / n * i);

        result = getZeroNewton(subCell, f);
        if( result[1] == 0. )
          return result[0];
      }
    }

  }

  return -1;
}

std::vector<std::complex<double>> getZeroNewton(solitonMap cell, std::vector<std::complex<double>> (*f)(std::complex<double>)) {

  double EPSILON = 1e-10;
  std::complex<double> zprev( cell.startPoint.real() + cell.boxSize * 1. / 3., cell.startPoint.imag() + cell.boxSize * 1. / 3. );
  std::complex<double> znext( cell.startPoint.real() + cell.boxSize * 2. / 3., cell.startPoint.imag() + cell.boxSize * 2. / 3. );

  int count = 0;
  while( isInside(cell, znext) && std::abs(zprev - znext) > EPSILON ) {
    std::complex<double> ztemp = znext;
    znext = znext - f(znext)[0]*(znext - zprev)/(f(znext)[0] - f(zprev)[0]); // [0] is coefficent 'a' from function Signal::calculateCoefficientsParallel
    zprev = ztemp;

    if( count > 1000000 )
      throw std::logic_error("Out of time for Newton method");
    count++;
  }

  std::vector<std::complex<double>> result(2);
  if( isInside(cell, znext) ) {
    result[0] = znext;
    result[1] = 0.;
  }
  else {
    result[0] = 0.;
    result[1] = -1.;
  }

  return result;
}

bool isBorder(solitonMap cell, std::complex<double> point) {
  if( ( ( cell.startPoint.real() == point.real() || cell.startPoint.real() + cell.boxSize == point.real() ) &&
      ( point.imag() >= cell.startPoint.imag() && point.imag() <= cell.startPoint.imag() + cell.boxSize ) ) ||
      ( ( cell.startPoint.imag() == point.imag() || cell.startPoint.imag() + cell.boxSize == point.imag() ) &&
      ( point.real() >= cell.startPoint.real() && point.real() <= cell.startPoint.real() + cell.boxSize ) )
    )
    return true;
  else
    return false;
}

bool isInside(solitonMap cell, std::complex<double> point) {
  if( ( point.imag() >= cell.startPoint.imag() && point.imag() <= cell.startPoint.imag() + cell.boxSize ) &&
      ( point.real() >= cell.startPoint.real() && point.real() <= cell.startPoint.real() + cell.boxSize )
    )
    return true;
  else
    return false;
}

#endif
