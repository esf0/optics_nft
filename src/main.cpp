#include "signal.h"
#include "ofdmsignal.h"
#include "satsumayajimasignal.h"
#include "wdmsignal.h"
#include <chrono>

int main(void) {
  std::cout << "Hi. It is test program for Signal class\n" << std::endl;

  std::cout << "\nFirst test\n";
  Signal testSignal;
  testSignal.printParameters();
  // testSignal.printToFile("test0.txt");

  std::cout << "\nSecond test\n";
  OfdmSignal testOfdmSignal(1., "l1", 10., pow(2,10), "qpsk", 16, 1024, 1./128.);
  testOfdmSignal.printParameters();
  //testOfdmSignal.drawSignal();

  std::cout << "\nThird test\n";
  SatsumaYajimaSignal testSY(2.7, 32, 128);
  //testSY.drawSignal();
  testSY.printParameters();
  std::vector<std::complex<double>> testSYSignal(testSY.getSignal());
  testSY.fourierCollocation();

  //for ( int i = 0; i < testSY.getNSignal(); i++ ) {
  //  std::cout << testSYSignal[i] << " ";
  //}
  //std::cout << std::endl;

  // std::vector<double> resultSY = testSY.getNSolitonsBox("rect", 2.0, pow(2, 12), -1.0);
  // std::cout << "OMP_NUM_THREADS = " << OMP_NUM_THREADS << std::endl;
  // std::cout << "N = " << resultSY[0] << " error = " << resultSY[1] << " real arg = " << resultSY[2] << std::endl;

  // std::vector<solitonMap> map = testSY.getSolitonMap("rect", 4.0, pow(2,12), -1.5);
  // std::cout << map[0].N << " " << map[0].boxSize << " " << map[0].startPoint << std::endl;
  // std::cout << map[1].N << " " << map[1].boxSize << " " << map[1].startPoint << std::endl;
  // std::cout << map[2].N << " " << map[2].boxSize << " " << map[2].startPoint << std::endl;

  // std::vector<std::complex<double>> eigenvalues = testSY.findSolitonsCouchy();
  // for( auto it = eigenvalues.begin(); it != eigenvalues.end(); it++ )
  //   std::cout << *it << std::endl;


  // int nSpecBord = pow ( 2, 11 );
  // int specBord = pow( 2, 7 );
  // int nSpec = pow( 2, 14 );
  // int specBox = pow( 2, 4 );
  // double dSpec = (double)specBox/nSpec;
  // double dSpecBord = (double)specBord/nSpecBord;
  // std::vector<std::complex<double>> xi(nSpec + 2*nSpecBord);
  // for ( int i = 0; i < nSpecBord; i++ ) {
  //   xi[i] = -specBord + i*dSpecBord - specBox/2;
  // }
  //
  // for ( int i = nSpecBord; i < nSpec + nSpecBord; i++ ) {
  //   xi[i] = -specBox/2 + (i - nSpecBord)*dSpec;
  // }
  //
  // for ( int i = nSpec + nSpecBord; i <nSpec + 2*nSpecBord; i++ ) {
  //   xi[i] = (i - nSpec - nSpecBord)*dSpecBord + specBox/2;
  // }
  //
  // std::vector<int> testOut;
  //std::cout << "create test out\n";
  //testOut = testSY.nSolitonsCauchy( xi );
  //std::cout << "N = " << testOut[0] << " ; Error = " << testOut[1] << std::endl;

  /*std::cout << "\nFourth test\n";
  auto chBegin = std::chrono::high_resolution_clock::now();
  OfdmSignal testOfdm2(32, "l1", 21.5, 1024, "16qam", 1024, 1024, 1./128);
  testOfdm2.printParameters();
  testOfdm2.printToFile("test.txt");
  //testOfdm2.calculateCoefficients(0.1);
  testOut = testOfdm2.nSolitonsCauchyParallel( xi );
  std::cout << "N = " << testOut[0] << " ; Error = " << testOut[1] << std::endl;
  auto chEnd = std::chrono::high_resolution_clock::now();
  std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(chEnd-chBegin).count() << "ms" << std::endl;
  */

  // std::cout << "\nFourth test\n\n";
  // double testM1[10] = {1,0,1,1,1,1,1,1,1,1};
  // double testM2[10] = {0,0,1,1,1,1,1,1,0,1};
  // double testM3[9] = {1,1,1,2,0,0,2,0,0};
  // double testM4[3] = {1,1,1};
  // double testM5[3] = {0,0,0};
  //
  // for ( int i = 0; i < 10; i++ )
  //   std::cout << testM1[i] << " ";
  // std::cout << std::endl;
  // for ( int i = 0; i < 10; i++ )
  //   std::cout << testM2[i] << " ";
  // std::cout << std::endl;
  // for ( int i = 0; i < 3; i++ ) {
  //   std::cout << testM3[i*3] << " " << testM3[i*3+1] << " " << testM3[i*3+2] << std::endl;
  // }
  // std::cout << std::endl;
  //
  // double testMRes;
  // testMRes = cblas_dasum(10, testM2, 1);
  // std::cout << "testRes " << testMRes << std::endl;
  //
  // cblas_dcopy(10, testM1, 1, testM2, 1);
  // for ( int i = 0; i < 10; i++ )
  //   std::cout << testM1[i] << " ";
  // std::cout << std::endl;
  // for ( int i = 0; i < 10; i++ )
  //   std::cout << testM2[i] << " ";
  // std::cout << std::endl;
  //
  // double* alpha = new(double);
  // double* beta = new(double);
  // *alpha = 1.;
  // *beta = 1.;
  // cblas_dgemv(CblasRowMajor, CblasNoTrans, 3, 3, *alpha, testM3, 3, testM4, 1, *beta, testM5, 1);
  // for ( int i = 0; i < 3; i++ )
  //   std::cout << testM5[i] << " ";
  // std::cout << std::endl;
  //
  // std::cout << "\nFifth test\n\n";
  // std::complex<double> zero(0, 0);
  // std::cout << "zero = " << zero << std::endl;
  // std::cout << "N = " << testOfdmSignal.getNSignal() << std::endl;
  // testOfdmSignal.addLateral(0, pow(2, 10));
  // std::cout << "N = " << testOfdmSignal.getNSignal() << std::endl;
  // // testOfdmSignal.drawSignal();
  //
  // std::cout << "\nSix[th] test\n\n";
  // WdmSignal wdmSignal( (int)pow(2, 10), 10., 3., "l2", 8.3, "qpsk", 1 );
  // wdmSignal.drawSignal();

  std::cout << "Main program end" << std::endl;
  std::cout << "The very end" << std::endl;
  return 0;
}
