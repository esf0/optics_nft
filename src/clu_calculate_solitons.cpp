#include "signal.h"
#include "ofdmsignal.h"
#include "satsumayajimasignal.h"
#include "nftanalyse.h"
#include <chrono>

int main(int argc, char** argv) {

  int nSamples = 1;
  // double timeRange = 1.;
  std::string normType("l1");
  double normValue = 1.;
  //int nData = (int)pow(2, 10);
  std::string modType("qpsk");
  int nSub = 128;
  //int nFFT = 1024;
  //double prefix = 1./128;

  if ( argc <= 1 ) {
    // std::cout << "Error: require input arguments\n";
    std::cerr << "Error: require input arguments\n";
    return -1;
  }
  else {
    nSamples = atoi( argv[1] );
    normType = argv[2];
    normValue = atof( argv[3] );
    modType = argv[4];
    nSub = atoi( argv[5] );
    std::cout << "nSamples = " << nSamples << std::endl;
    std::cout << "normType = " << normType << std::endl;
    std::cout << "normValue = " << normValue << std::endl;
    std::cout << "modType = " << modType << std::endl;
    std::cout << "nSub = " << nSub << std::endl;
  }

  // Create vector xi to calculate N solitons
  // Xi is a grid in spectral space on real axe

  int N[3] = { (int)pow( 2, 11 ), (int)pow( 2, 12 ), (int)pow( 2, 11 ) };
  double L[3] = { pow( 2, 7 ), pow( 2, 4 ), pow( 2, 7 ) };
  // int N[3] = {8, 8, 8};
  // double L[3] = {2., 1., 2.};
  std::vector<std::complex<double>> xi = gridXi( 3, N, L );
  std::vector<double> out;
  std::vector<int> nSolitons;
  std::vector<int> err;

  std::string infoFileName("info.txt");
  std::ofstream resultFile("result.txt", std::ios::out | std::ios::app );
  if( resultFile.is_open() ) {
    // std::cout << "File opened\n";
    for ( int i = 0; i < nSamples; i++ ) {
      auto chBegin = std::chrono::high_resolution_clock::now();

      OfdmSignal ofdmSignal(1., normType, normValue, (int)pow(2,10), modType, nSub, 1024, 1./128.);
      ofdmSignal.addLateral(0, pow(2, 4));
      ofdmSignal.printToFile( infoFileName, std::ios::out | std::ios::app );
      out = ofdmSignal.nSolitonsCauchyParallel(xi);
      nSolitons.push_back( out[0] );
      err.push_back( out[1] );

      auto chEnd = std::chrono::high_resolution_clock::now();
      resultFile << out[0] << " " << out[1] << " " << out[2] << " " <<
                    std::chrono::duration_cast<std::chrono::milliseconds>(chEnd-chBegin).count() <<
                    std::endl;
    }
    resultFile.close();
    // std::cout << "File closed\n";
  }

  return 0;
}
