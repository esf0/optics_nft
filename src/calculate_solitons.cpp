#include "signal.h"
#include "ofdmsignal.h"
#include "satsumayajimasignal.h"
#include <chrono>

int main(void) {
  std::cout << "Hi. It is test program to calculate solitons number\n" << std::endl;

  //std::cout << "\nSecond test\n";
  //OfdmSignal ofdmSignal(1., "l1", 10., pow(10,3), "qpsk", 16, 1024, 1./128.);
  //ofdmSignal.printParameters();
  //ofdmSignal.drawSignal();

  int nSpecBord = pow ( 2, 11 );
  int specBord = pow( 2, 7 );
  int nSpec = pow( 2, 12 );
  int specBox = pow( 2, 4 );
  double dSpec = (double)specBox/nSpec;
  double dSpecBord = (double)specBord/nSpecBord;
  std::vector<std::complex<double>> xi(nSpec + 2*nSpecBord);

  for ( int i = 0; i < nSpecBord; i++ ) {
    xi[i] = -specBord + i*dSpecBord - specBox/2;
  }

  for ( int i = nSpecBord; i < nSpec + nSpecBord; i++ ) {
    xi[i] = -specBox/2 + (i - nSpecBord)*dSpec;
  }

  for ( int i = nSpec + nSpecBord; i <nSpec + 2*nSpecBord; i++ ) {
    xi[i] = (i - nSpec - nSpecBord)*dSpecBord + specBox/2;
  }

  std::cout << "Calculate number of solitons for OFDM signal\n";
  auto chBegin = std::chrono::high_resolution_clock::now();
  std::vector<int> out;
  std::vector<int> N;
  std::vector<int> err;
  int num = 0;


  std::ofstream afile("soliton.txt", std::ios::out);
  if (afile.is_open()) {
    std::cout << "File opened\n";
    for ( int i = 0; i < 100; i++ ) {
      OfdmSignal ofdmSignal(1., "l1", 8.5, pow(2,10), "qpsk", 128, 1024, 1./128.);
      ofdmSignal.addLateral(0, pow(2, 4));
      out = ofdmSignal.nSolitonsCauchyParallel(xi);
      N.push_back( out[0] );
      err.push_back( out[1] );
      afile << out[0] << " " << out[1] << std::endl;
    }
    afile.close();
    std::cout << "File closed\n";
  }
  for( auto it = N.begin(); it != N.end(); ++it ) {
    if( *it > 1 )
      num++;
  }
  std::cout << "Probability: " << (double)num/N.size() << std::endl;

  //std::cout << "N = " << out[0] << " ; Error = " << out[1] << std::endl;
  auto chEnd = std::chrono::high_resolution_clock::now();
  std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(chEnd-chBegin).count()
            << "ms" << std::endl;

  /*
  SatsumaYajimaSignal signalSY(2.7, 32, 128);
  signalSY.drawSignal();
  signalSY.printParameters();

  std::cout << "Calculate number of solitons for SY signal\n";
  out = signalSY.nSolitonsCauchy( xi );
  std::cout << "N = " << out[0] << " ; Error = " << out[1] << std::endl;
  */

  std::cout << "Main program end" << std::endl;
  std::cout << "The very end" << std::endl;
  return 0;
}
