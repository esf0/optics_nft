#include "signal.h"
#include "ofdmsignal.h"
#include "satsumayajimasignal.h"
#include <chrono>

int main(int argc, char** argv) {

  // OfdmSignal testOfdmSignal(1., "l1", 10., pow(2,10), "qpsk", 16, 1024, 1./128.);
  OfdmSignal signal_1(1., "l1", 8.5, pow(2, 10), "qpsk", 128, 1024, 1./128);
  OfdmSignal signal_2(16.125, "l1", 8.5, pow(2, 10), "qpsk", 128, 1024, 1./128);

  std::cout << signal_1.getNormL1(true) << std::endl;
  std::cout << signal_1.getNormL1() << std::endl;
  std::cout << signal_1.getNormL2(true) << std::endl;
  std::cout << signal_1.getNormL2() << std::endl;

  std::cout << "*-----------------------------*" << std::endl;

  signal_1.setNewTimeBoxSize(10.);
  std::cout << signal_1.getNormL1(true) << std::endl;
  std::cout << signal_1.getNormL1() << std::endl;
  std::cout << signal_1.getNormL2(true) << std::endl;
  std::cout << signal_1.getNormL2() << std::endl;

  std::cout << "*-----------------------------*" << std::endl;

  signal_1.setNewTimeBoxSize(16.125);
  std::cout << signal_1.getNormL1(true) << std::endl;
  std::cout << signal_1.getNormL1() << std::endl;
  std::cout << signal_1.getNormL2(true) << std::endl;
  std::cout << signal_1.getNormL2() << std::endl;

  std::cout << "*-----------------------------*" << std::endl;

  signal_1.setNewTimeBoxSize(1.);
  std::cout << signal_1.getNormL1(true) << std::endl;
  std::cout << signal_1.getNormL1() << std::endl;
  std::cout << signal_1.getNormL2(true) << std::endl;
  std::cout << signal_1.getNormL2() << std::endl;

  std::cout << "*-----------------------------*" << std::endl;

  std::cout << signal_2.getNormL1(true) << std::endl;
  std::cout << signal_2.getNormL1() << std::endl;
  std::cout << signal_2.getNormL2(true) << std::endl;
  std::cout << signal_2.getNormL2() << std::endl;
  std::cout << signal_2.getNSignal() << std::endl;
  std::cout << signal_2.getTimeBoxSize() << std::endl;

  std::cout << "*-----------------------------*" << std::endl;

  signal_1.addLateral( 0, 1016/2 );
  std::cout << signal_1.getNormL1(true) << std::endl;
  std::cout << signal_1.getNormL1() << std::endl;
  std::cout << signal_1.getNormL2(true) << std::endl;
  std::cout << signal_1.getNormL2() << std::endl;

  std::cout << "*-----------------------------*" << std::endl;

  signal_2.addLateral( 0, 1016/2 );
  std::cout << signal_2.getNormL1(true) << std::endl;
  std::cout << signal_2.getNormL1() << std::endl;
  std::cout << signal_2.getNormL2(true) << std::endl;
  std::cout << signal_2.getNormL2() << std::endl;
  std::cout << signal_2.getNSignal() << std::endl;
  std::cout << signal_2.getTimeBoxSize() << std::endl;



  return 0;
}
