// This file contains realization of subclass OfdmSignal
// author: Egor Sedov
// date: 29.06.2018
// e-mail: egor.sedoff@gmail.com

#ifndef OFDM_SIGNAL_CPP
#define OFDM_SIGNAL_CPP

#include "ofdmsignal.h"

OfdmSignal::OfdmSignal() : Signal() {
  modType = "empty";
  nSub = 0;
  nFFT = 0;
  nPrefix = 0;
}

OfdmSignal::OfdmSignal(double timeBoxSize, std::string normType, double normValue, int nData,
  std::string modType, int nSub, int nFFT, double nPrefix) : Signal() {

  this->setTimeBoxSize(timeBoxSize);
  this->setSignalType("ofdm");
  this->setSignal(OfdmSignal::getOfdm( nData, modType, nSub, nFFT, nPrefix));
  if ( normType == "l1" || normType == "L1" ) {
    this->setNormL1(normValue);
    this->setNormType("l1");
  }
  if ( normType == "l2" || normType == "L2" ) {
    this->setNormL2(normValue);
    this->setNormType("l2");
  }
  this->rescaleSignal();


  this->modType = modType;
  this->nSub = nSub;
  this->nFFT = nFFT;
  this->nPrefix = nPrefix;
}

std::vector<std::complex<double>> OfdmSignal::getRandomConstellation(std::string modType, int size, int randomIn) {

  if ( modType != "qpsk" && modType != "16qam" && modType != "64qam" && modType != "1024qam" ) {
    std::cout << "********************************" << std::endl;
    std::cout << "Ofdm error: no such modulation type" << std::endl;
    std::cout << "********************************" << std::endl;
  }

  std::mt19937 gen(time(0) + randomIn);
  std::discrete_distribution<> discrQPSK({0, 1});
  std::discrete_distribution<> discr16QAM({0, 1, 0, 1});
  std::discrete_distribution<> discr64QAM({0, 1, 0, 1, 0, 1, 0, 1});
  std::discrete_distribution<> discr1024QAM(
    {0, 1, 0, 1, 0, 1, 0, 1,
      0, 1, 0, 1, 0, 1, 0, 1,
      0, 1, 0, 1, 0, 1, 0, 1,
      0, 1, 0, 1, 0, 1, 0, 1});

  std::discrete_distribution<> signD({1,1});

  std::vector<std::complex<double>> result;
  for ( int i = 0; i < size; i++ ) {
    // I use 2 signs to move between quarters
    // +- | ++
    // -------
    // -- | -+
    int signOne;
    int signTwo;

    if ( signD(gen) == 0 )
      signOne = -1;
    else
      signOne = 1;

    if ( signD(gen) == 0 )
      signTwo = -1;
    else
      signTwo = 1;

    double realPart;
    double imagPart;
    if( modType == "qpsk" ) {
      realPart = discrQPSK(gen);
      imagPart = discrQPSK(gen);
    }
    if( modType == "16qam" ) {
      realPart = discr16QAM(gen);
      imagPart = discr16QAM(gen);
    }
    if( modType == "64qam" ) {
      realPart = discr64QAM(gen);
      imagPart = discr64QAM(gen);
    }
    if( modType == "1024qam" ) {
      realPart = discr1024QAM(gen);
      imagPart = discr1024QAM(gen);
    }

    std::complex<double> complexNumber(realPart*signOne,imagPart*signTwo);
    result.push_back(complexNumber);
  }

  return result;
}

std::vector<std::complex<double>> OfdmSignal::getOfdm(int nData,
  std::string modType, int nSub, int nFFT, double nPrefix) {

  unsigned int nSymbols = ceil(nData/(nFFT*(1 + nPrefix)));
  unsigned int prefixSize = nFFT*nPrefix;
  unsigned int signalSize = nSymbols*(nFFT + prefixSize);

  std::vector<std::complex<double>> data(nSub*nSymbols);
  int iData = 0;

  std::vector<std::complex<double>> outSignal(signalSize); // Empty output signal
  int iOutSignal = 0;

  for ( unsigned int k = 0; k < nSymbols; k++ ) {
    std::vector<std::complex<double>> vecInput(nSub); // Create random distribution
    vecInput = OfdmSignal::getRandomConstellation(modType, nSub, k);

    size_t align = sizeof(std::complex<double>);

    std::vector<std::complex<double>> vecInFFT(nFFT);
    Array::array1<std::complex<double>> arrayFFT(nFFT, align);
    for ( int i = 0; i < nSub/2; i++ ) {
      arrayFFT[i] = vecInput[i];
      arrayFFT[nFFT - nSub/2 + i] = vecInput[i + nSub/2];
    }

    //std::cout << "\nArray:\n" << arrayFFT <<std::endl; //Uncomment it to print FFT input

    //omp_set_num_threads(NUM_THREADS); //Doesn't work
    //fftwpp::fftw::maxthreads = get_max_threads(); //Doesn't work

    fftwpp::fft1d Backward(1,arrayFFT);
    Backward.fftNormalized(arrayFFT); //It is the same as ifft in Matlab

    for ( int i = 0; i < nSub; i++ )
      data[i + iData] = vecInput[i];
    for ( unsigned int i = 0; i < prefixSize; i++ )
      outSignal[i + iOutSignal] = arrayFFT[i + nFFT - prefixSize];
    for ( int i = 0; i < nFFT; i++ )
      outSignal[i + prefixSize + iOutSignal] = arrayFFT[i];

    iData += nSub;
    iOutSignal = iOutSignal + nFFT + prefixSize;
  }

  //Uncomment it to print output ofdm signal
  //std::cout << "\nOutput ofdm signal:\n";
  //std::vector<std::complex<double>>::iterator it;
  //for(it = outSignal.begin(); it < outSignal.end(); ++it)
  //  std::cout << *it << " ";
  //std::cout << std::endl;

  this->setData(data);
  return outSignal;
}

void OfdmSignal::printParameters() {
  Signal::printParameters();
  std::cout << "\n***** OFDM PARAMETERS *****\n" << std::endl
    << "OFDM modulation type : " << modType << std::endl
    << "Number of subcarriers : " << nSub << std::endl
    << "Number of Fourier modes : " << nFFT << std::endl
    << "Cycle prefix : " << nPrefix << std::endl;
}

int OfdmSignal::printToFile(std::string file, std::ios_base::openmode mode ) {
  std::ofstream outFile;
  outFile.open(file, mode);
  outFile << this->getSignalType() << std::endl
    << this->getNormType() << std::endl
    << this->getNormL1() << std::endl
    << this->getNormL2() << std::endl
    << this->getPeakToAverage() << std::endl
    << this->getTimeBoxSize() << std::endl
    << modType << std::endl
    << nSub << std::endl
    << nFFT << std::endl
    << nPrefix << std::endl
    << this->getNData() << std::endl;

  std::vector<std::complex<double>> data = getData();
  std::vector<std::complex<double>> signal = getSignal();
  for ( int i = 0; i < this->getNData(); i++ ) {
    outFile << data[i].real() << " " << data[i].imag() << std::endl;
  }
  outFile << this->getNSignal() << std::endl;
  for ( int i = 0; i < this->getNSignal(); i++ ) {
    outFile << signal[i].real() << " " << signal[i].imag() << std::endl;
  }
  outFile.close();

  return 0;
}

#endif
