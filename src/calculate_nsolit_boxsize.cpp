#include "signal.h"
#include "ofdmsignal.h"
#include "nftanalyse.h"
#include <chrono>

int main(int argc, char** argv) {

  int nSamples = 20;

  // Create vector xi to calculate N solitons
  // Xi is a grid in spectral space on real axe

  int N[3] = { (int)pow( 2, 14 ), (int)pow( 2, 15 ), (int)pow( 2, 14 ) };
  double L[3] = { pow( 2, 8 ), pow( 2, 4 ), pow( 2, 8 ) };

  std::vector<std::complex<double>> xi = gridXi( 3, N, L );
  std::vector<double> out;
  std::vector<int> nSolitons;
  std::vector<int> err;

  std::ofstream resultFile("result_boxsize.txt", std::ios::out | std::ios::app );

  OfdmSignal ofdmSignal(1., "l1", 10., (int)pow(2,10), "qpsk", 128, 1024, 0.);
  ofdmSignal.addLateral(0, pow(2, 7));

  if( resultFile.is_open() ) {
    for ( int i = 0; i < nSamples; i++ ) {
      auto chBegin = std::chrono::high_resolution_clock::now();

      ofdmSignal.setNewTimeBoxSize( (i + 1)*1.*1.25 );
      out = ofdmSignal.nSolitonsCauchyParallel(xi);
      nSolitons.push_back( out[0] );
      err.push_back( out[1] );

      auto chEnd = std::chrono::high_resolution_clock::now();
      resultFile << out[0] << " " << out[1] << " " << out[2] << " " <<
                    std::chrono::duration_cast<std::chrono::milliseconds>(chEnd-chBegin).count() <<
                    std::endl;
    }
    resultFile.close();
  }

  return 0;
}
