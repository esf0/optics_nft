#include "signal.h"

int main(int argc, char** argv) {

  if( argc <= 1 ) {
    std::cout << "Require input file name\n";
    return 1;
  }

  std::vector<int> nSolitons, errorKey, time;
  std::vector<double> deltaArg;

  int tempN, tempError, tempTime;
  double tempDelta;

  int total = 0;
  int bad = 0;
  int good = 0;
  std::ifstream inputFile( argv[1] );
  if( inputFile.is_open() ) {
    while( inputFile >> tempN && inputFile >> tempError && inputFile >> tempDelta && inputFile >> tempTime ) {
      // std::cout << tempN << " " << tempError << " " << tempDelta << " "<< tempTime << std::endl;
      if( tempError != -1 ) {
        nSolitons.push_back( tempN );
        time.push_back( tempTime );
        good++;
      }
      else {
        bad++;
      }
      total++;
    }
  }
  inputFile.close();
  //std::cout << "Close input file" << std::endl;

  std::cout << "Total = " << total << " with " << good << " good and " << bad << " bad\n";

  double mean = 0.;
  double exist = 0.;
  for( auto it = nSolitons.begin(); it != nSolitons.end(); it++ ) {
    mean += (double)*it / nSolitons.size();
    if( *it > 0 )
      exist += 1. / nSolitons.size();
  }
  std::cout << "Mean value = " << mean << std::endl;
  std::cout << "Exist ratio = " << exist << std::endl;

  return 0;
}
