#include "signal.h"
#include "ofdmsignal.h"
#include "satsumayajimasignal.h"
#include <chrono>

std::vector<std::complex<double>> gridXi( int n, int* N, double* L) {
  int totalN = 0;
  double totalL = 0;
  for( int i = 0; i < n; i++ ) {
    totalN += N[i];
    totalL += L[i];
  }
  std::vector<std::complex<double>> xi(totalN);

  int currentN = 0;
  double currentL = -totalL/2.;
  for( int i = 0; i < n; i++ ) {
    double dl = L[i]/N[i];
    for( int j = 0; j < N[i]; j++ ) {
      xi[currentN + j] = currentL + dl*j;
    }
    currentN += N[i];
    currentL += L[i];
  }

  return xi;
}

int main(int argc, char** argv) {

  int nSamples = 1;

  // Create vector xi to calculate N solitons
  // Xi is a grid in spectral space on real axe

  int N[3] = { (int)pow( 2, 12 ), (int)pow( 2, 13 ), (int)pow( 2, 12 ) };
  double L[3] = { pow( 2, 7 ), pow( 2, 4 ), pow( 2, 7 ) };
  // int N[3] = {8, 8, 8};
  // double L[3] = {2., 1., 2.};
  std::vector<std::complex<double>> xi = gridXi( 3, N, L );
  std::vector<double> out;
  std::vector<int> nSolitons;
  std::vector<int> err;

  std::string infoFileName("../source/info.txt");
  std::ofstream resultFile("../source/result.txt", std::ios::out | std::ios::app );
  if( resultFile.is_open() ) {
    // std::cout << "File opened\n";
    for ( int i = 0; i < nSamples; i++ ) {
      auto chBegin = std::chrono::high_resolution_clock::now();

      std::vector<std::complex<double>> input;
      // std::string line;
      std::ifstream inputFile("../source/16QAM_128_8.75.txt");
      if( inputFile.is_open() ) {
        double re, im;
        while( inputFile >> re && inputFile >> im ) {
          std::complex<double> tempDouble(re, im);
          // std::cout << line << std::endl;
          input.push_back( tempDouble );
        }
      }
      inputFile.close();
      std::cout << "Close input file" << std::endl;

      Signal signal( input, 32. );
      // std::cout << signal.getNormL1(true) << std::endl;

      // OfdmSignal ofdmSignal(1., normType, normValue, (int)pow(2,10), modType, nSub, 1024, 1./128.);
      // ofdmSignal.addLateral(0, pow(2, 4));
      // ofdmSignal.printToFile( infoFileName, std::ios::out | std::ios::app );
      out = signal.nSolitonsCauchyParallel( xi );
      nSolitons.push_back( out[0] );
      err.push_back( out[1] );

      auto chEnd = std::chrono::high_resolution_clock::now();
      resultFile << out[0] << " " << out[1] << " " << out[2] << " " <<
                    std::chrono::duration_cast<std::chrono::milliseconds>(chEnd-chBegin).count() <<
                    std::endl;
    }
    resultFile.close();
    // std::cout << "File closed\n";
  }

  return 0;
}
