// This file contains realization of class Signal
// author: Egor Sedov
// date: 29.06.2018
// e-mail: egor.sedoff@gmail.com

#include "signal.h"

// #define MKL_Complex16 std::complex<double>

Signal::Signal() {
  signalType = "empty";
  nSignal = 0;
  nData = 0;
  normType = "empty";
  normL1 = 0;
  normL2 = 0;
  peakToAverage = 0;
  timeBoxSize = 1.;
}

Signal::Signal(std::vector<std::complex<double>>& signal, double timeBoxSize) {
  this->setSignal( signal );
  this->setTimeBoxSize( timeBoxSize );
  this->setSignalType( "from_data" );
  this->setNData( this->getNSignal() );
  this->setNormL1( this->getNormL1(true) );
  this->setNormL2( this->getNormL2(true) );
  this->calculatePeakToAverage();
}

//Signal::Signal(int nData, char* modType, int nSub, int normType, double normValue) {
//
//}

//Signal::Signal(&Signal) {
//
//}

//~Signal::Signal() {
//
//}


void Signal::setSignalType(std::string signalType) {
  this->signalType = signalType;
}

void Signal::setSignal(std::vector<std::complex<double>> signal) {
  this->signal = signal;
  this->nSignal = signal.size();
}

void Signal::setData(std::vector<std::complex<double>> data) {
  this->data = data;
  this->setNData(data.size());
}

void Signal::setNData(int nData) {
  this->nData = nData;
}

void Signal::setNormL1(double normL1) {
  this->normL1 = normL1;
}

void Signal::setNormL2(double normL2) {
  this->normL2 = normL2;
}

// void Signal::setNewNormL1(double normL1) {
//   this->setNormL1(normL1);
//   this->rescaleSignal();
// }

void Signal::setNormType(std::string normType) {
  this->normType = normType;
}

void Signal::setTimeBoxSize(double timeBoxSize) {
  this->timeBoxSize = timeBoxSize;
}

void Signal::setNewTimeBoxSize(double timeBox) {
  this->timeBoxSize = timeBox;
  this->rescaleSignal();
}

std::vector<std::complex<double>> Signal::getSignal() {
  return this->signal;
}

std::vector<std::complex<double>> Signal::getData() {
  return this->data;
}

std::string Signal::getSignalType() {
  return this->signalType;
}

int Signal::getNSignal() {
  return this->nSignal;
}

int Signal::getNData() {
  return this->nData;
}

double Signal::getNormL1() {
  return this->normL1;
}

double Signal::getNormL1(bool calc) {
  if ( calc ) {
    double integral = 0;
    double dt = timeBoxSize/nSignal;
    for ( int i = 0; i < nSignal; i++ ) {
      integral += abs(signal[i])*dt;
    }
    return integral;
  }
  else {
    return this->normL1;
  }
}

double Signal::getNormL2() {
  return this->normL2;
}

double Signal::getNormL2(bool calc) {
  if ( calc ) {
    double integral = 0;
    double dt = timeBoxSize/nSignal;
    for ( int i = 0; i < nSignal; i++ ) {
      integral += pow(abs(signal[i]),2)*dt;
    }
    return integral;
  }
  else {
    return this->normL2;
  }
}

double Signal::getPeakToAverage() {
  return this->peakToAverage;
}

std::string Signal::getNormType() {
  return this->normType;
}

double Signal::getTimeBoxSize() {
  return this->timeBoxSize;
}

std::vector<std::complex<double>> Signal::getCoefXi() {
  return this->coefXi;
}

std::vector<std::complex<double>> Signal::getCoefA() {
  return this->coefA;
}

double Signal::calculatePeakToAverage() {
  double average = 0;
  double peak = 0;
  for ( int i = 0; i < nSignal; i++ ) {
    if ( abs(signal[i]) > peak )
      peak = abs(signal[i]);
    average += abs(signal[i])/nSignal;
  }
  double value = peak/average;

  this->peakToAverage = value;
  return value;
}

int Signal::rescaleSignal() {
  if ( normType == "empty" || normType == "Empty" ) {
    std::cout << "********************************" << std::endl;
    std::cout << "Rescaling error: empty norm type" << std::endl;
    std::cout << "********************************" << std::endl;
    return 3;
  }

  unsigned int out = 0;

  if ( normType == "l1" || normType == "L1" ) {

    if ( normL1 == 0 ) {
      std::cout << "********************************" << std::endl;
      std::cout << "Rescaling error: l1 norm is 0" << std::endl;
      std::cout << "********************************" << std::endl;
      return 11;
    }

    double averageL1 = getNormL1(1);
    if ( averageL1 != normL1 ) {
      double ampl = normL1 / averageL1;
      for ( int i = 0; i < nSignal; i++ )
        signal[i] *= ampl;
      out = 1;
    }
    // this->setNormL2(this->getNormL2(1));
    out = 10;
  }

  if ( normType == "l2" || normType == "L2" ) {

    if ( normL2 == 0 ) {
      std::cout << "********************************" << std::endl;
      std::cout << "Rescaling error: l2 norm is 0" << std::endl;
      std::cout << "********************************" << std::endl;
      return 21;
    }

    double averageL2 = getNormL2(1);
    if ( averageL2 != normL2 ) {
      double ampl = sqrt( normL2 / averageL2 );
      for ( int i = 0; i < nSignal; i++ )
        signal[i] *= ampl;
      out = 2;
    }
    // this->setNormL1(this->getNormL1(1));
    out = 20;
  }

  this->setNormL1(this->getNormL1(1));
  this->setNormL2(this->getNormL2(1));
  this->calculatePeakToAverage();
  return out;
}

int Signal::addLateral(int type, int nPoints) {
  if( type == 0 ) {
    std::complex<double> zero(0, 0);
    auto it = this->signal.begin();
    this->signal.insert(it, nPoints, zero); //Insert nPoints elements to begin
    it = this->signal.end();
    this->signal.insert(it, nPoints, zero); //Insert nPoints elements to the end

    this->setTimeBoxSize( this->getTimeBoxSize()*(2*nPoints + this->getNSignal())/this->getNSignal() );
    this->nSignal = signal.size();

    //std::cout << "New norm L1: " << this->getNormL1(true)
    //          << " old norm L1: " << this->getNormL1() << std::endl;
    //std::cout << "New norm L2: " << this->getNormL2(true)
    //          << " old norm L2: " << this->getNormL2() << std::endl;
  }
  else {

  }
  return 0;
}

void Signal::printParameters() {
  std::cout << "\n***** SIGNAL PARAMETERS *****\n" << std::endl
    << "Signal type : " << signalType << std::endl
    << "Number of signal points : " << nSignal << std::endl
    << "Number of input data : " << nData << std::endl
    << "Value of L1 norm : " << normL1 << std::endl
    << "Value of L2 norm : " << normL2 << std::endl
    << "Peak to avarage ratio : " << peakToAverage << std::endl
    << "Size of time interval : " << timeBoxSize << std::endl;
}

int Signal::printSignalToFile(std::string file, std::ios_base::openmode mode) {
  std::ofstream outFile;
  outFile.open(file, mode);

  std::vector<std::complex<double>> signal = getSignal();
  double dt = this->getTimeBoxSize() / this->getNSignal();
  for ( int i = 0; i < this->getNSignal(); i++ ) {
    double t = dt*i - this->getTimeBoxSize()/2.;
    outFile << t << " "<< signal[i].real() << " " << signal[i].imag() << std::endl;
  }
  outFile.close();

  return 0;
}

int Signal::printToFile(std::string file, std::ios_base::openmode mode) {
  std::ofstream outFile;
  outFile.open(file, mode);
  outFile << this->getSignalType() << std::endl
    << this->getNormType() << std::endl
    << this->getNormL1() << std::endl
    << this->getNormL2() << std::endl
    << this->getPeakToAverage() << std::endl
    << this->getTimeBoxSize() << std::endl
    << this->getNData() << std::endl;

  std::vector<std::complex<double>> data = getData();
  std::vector<std::complex<double>> signal = getSignal();
  for ( int i = 0; i < this->getNData(); i++ ) {
    outFile << data[i].real() << " " << data[i].imag() << std::endl;
  }
  outFile << this->getNSignal() << std::endl;
  for ( int i = 0; i < this->getNSignal(); i++ ) {
    outFile << signal[i].real() << " " << signal[i].imag() << std::endl;
  }
  outFile.close();

  return 0;
}

void Signal::drawSignal() {
  #ifdef WIN32
    FILE* pipe = _popen(GNUPLOT_NAME, "w");
  #else
    FILE* pipe = popen(GNUPLOT_NAME, "w");
  #endif

  if( pipe == NULL ) {
    std::cout << "Error opening pipe to GNU plot." << std::endl;
    throw std::bad_exception();
  }
  else {
    fprintf(pipe, "plot '-' with lines\n");
    for(int i = 0; i < this->nSignal; i++)
      fprintf(pipe, "%f\n", std::abs(signal[i]));
    fprintf(pipe, "%s\n", "e");
    fflush(pipe);

    // Wait for pressing of any key
    std::cin.clear();
    std::cin.ignore(std::cin.rdbuf()->in_avail());
    std::cin.get();
  }

  #ifdef WIN32
    _pclose(pipe);
  #else
    pclose(pipe);
  #endif
}

int Signal::directTIB() {
  return 0;
}

int Signal::inverseTIB() {
  return 0;
}

std::vector<std::complex<double>> Signal::calculateCoefficients(std::complex<double> xi) {
  double dt = this->timeBoxSize/this->nSignal;

  std::complex<double> imagOne(0.,1.);
  std::complex<double> t11(0.,0.);
  std::complex<double> t12(0.,0.);
  std::complex<double> t21(0.,0.);
  std::complex<double> t22(0.,0.);

  std::complex<double> p11(1.,0.);
  std::complex<double> p12(0.,0.);
  std::complex<double> p21(0.,0.);
  std::complex<double> p22(1.,0.);

  for ( int i = 0; i < this->nSignal; i++ ) {
    std::complex<double> kap = sqrt( -pow( abs( this->signal[i] ), 2 ) - pow( xi, 2 ) );
    t11 = cosh(kap*dt) - imagOne*xi / kap*sinh(kap*dt);
    t12 = this->signal[i] / kap*sinh(kap*dt);
    t21 = -std::conj(this->signal[i]) / kap*sinh(kap*dt);
    t22 = cosh(kap*dt) + imagOne*xi / kap*sinh(kap*dt);

    std::complex<double> pPrev11(p11);
    std::complex<double> pPrev12(p12);
    std::complex<double> pPrev21(p21);
    std::complex<double> pPrev22(p22);

    p11 = pPrev11*t11 + pPrev12*t21;
    p12 = pPrev11*t12 + pPrev12*t22;
    p21 = pPrev21*t11 + pPrev22*t21;
    p22 = pPrev21*t12 + pPrev22*t22;

  }

  this->coefXi.push_back( xi );
  this->coefA.push_back( p11*exp(imagOne*xi*this->timeBoxSize) );
  this->coefB.push_back( p21 );

  std::vector<std::complex<double>> coefficients(2);
  coefficients[0] = p11*exp(imagOne*xi*this->timeBoxSize);
  coefficients[1] = p21;

  return coefficients;
}

double Signal::deltaArg( std::vector<std::complex<double>> coefA ) {

  unsigned int size = coefA.size();

  double phi = 0;
  for ( unsigned int i = 0; i < size - 1; i++ ) {
    double iF = imag( coefA[i] );
    double iS = imag( coefA[i+1] );
    double rF = real( coefA[i] );
    double rS = real( coefA[i+1] );
    double dPhi = 0;

    if ( iF*iS < 0 && rF*rS < 0 ) {
      return -1;
    }
    if ( ( iF > 0 && iS > 0 ) ||
          ( iF < 0 && iS < 0 ) ||
          ( iF > 0 && iS < 0 && rF > 0 && rS > 0 ) ||
          ( iF < 0 && iS > 0 && rF > 0 && rS > 0 ) ) {
      dPhi = std::arg( coefA[i+1] ) - std::arg( coefA[i] );
    }
    if ( iF > 0 && iS < 0 && rF < 0 && rS < 0 )
      dPhi = std::arg( coefA[i+1] ) - std::arg( coefA[i] ) + 2*M_PI;
    if ( iF < 0 && iS > 0 && rF < 0 && rS < 0 )
      dPhi = std::arg( coefA[i+1] ) - std::arg( coefA[i] ) - 2*M_PI;

    phi += dPhi;
  }

  return phi;
}

std::vector<double> Signal::nSolitonsCauchy(std::vector<std::complex<double>> xi) {

  std::vector<std::complex<double>> coefficients(2);
  std::vector<std::complex<double>> a( xi.size() );


  for ( unsigned int i = 0; i < xi.size(); i++ ) {
    coefficients = this->calculateCoefficients( xi[i] );
    a[i] = coefficients[0];
  }

  std::vector<double> result(3);
  result[0] = 0.;
  result[1] = 0.;
  result[2] = 0.;

  double nSolitons = this->deltaArg(a);

  if ( nSolitons == -1 ) {
    std::cerr << "Error in argument calculation" << std::endl;
    result[1] = -1;
  }
  nSolitons /= (2*M_PI);
  double eps = 0.2;

  //std::cout << nSolitons <<std::endl;

  result[0] = round(nSolitons);
  result[2] = nSolitons;
  if( result[1] != -1 ) {
    if ( abs( nSolitons - result[0] ) < eps )
      result[1] = 0;
    else
      result[1] = 1;
  }

  return result;
}

std::vector<std::complex<double>> Signal::calculateCoefficientsParallel(std::complex<double> xi) {
  double dt = this->timeBoxSize/this->nSignal;

  std::complex<double> imagOne(0.,1.);
  std::complex<double> t11(0.,0.);
  std::complex<double> t12(0.,0.);
  std::complex<double> t21(0.,0.);
  std::complex<double> t22(0.,0.);

  std::complex<double> p11(1.,0.);
  std::complex<double> p12(0.,0.);
  std::complex<double> p21(0.,0.);
  std::complex<double> p22(1.,0.);

  for ( int i = 0; i < this->nSignal; i++ ) {
    std::complex<double> kap = sqrt( -pow( abs( this->signal[i] ), 2 ) - pow( xi, 2 ) );
    t11 = cosh(kap*dt) - imagOne*xi / kap*sinh(kap*dt);
    t12 = this->signal[i] / kap*sinh(kap*dt);
    t21 = -std::conj(this->signal[i]) / kap*sinh(kap*dt);
    t22 = cosh(kap*dt) + imagOne*xi / kap*sinh(kap*dt);

    std::complex<double> pPrev11(p11);
    std::complex<double> pPrev12(p12);
    std::complex<double> pPrev21(p21);
    std::complex<double> pPrev22(p22);

    p11 = pPrev11*t11 + pPrev12*t21;
    p12 = pPrev11*t12 + pPrev12*t22;
    p21 = pPrev21*t11 + pPrev22*t21;
    p22 = pPrev21*t12 + pPrev22*t22;

  }

  //this->coefXi.push_back( xi );
  //this->coefA.push_back( p11*exp(imagOne*xi*this->timeBoxSize) );
  //this->coefB.push_back( p21 );

  std::vector<std::complex<double>> coefficients(2);
  coefficients[0] = p11*exp(imagOne*xi*this->timeBoxSize);
  coefficients[1] = p21;

  return coefficients;
}

double Signal::deltaArgParallel( std::vector<std::complex<double>> coefA ) {

  //omp_set_num_threads(OMP_NUM_THREADS);
  unsigned int size = coefA.size();

  int error = 0;
  double phi = 0;
  #pragma omp parallel shared(coefA)
  #pragma for reduction (+:phi)
  for ( unsigned int i = 0; i < size - 1; i++ ) {
    double iF = imag( coefA[i] );
    double iS = imag( coefA[i+1] );
    double rF = real( coefA[i] );
    double rS = real( coefA[i+1] );
    double dPhi = 0;

    if ( iF*iS < 0 && rF*rS < 0 ) {
      error = -1;
    }
    if ( ( iF > 0 && iS > 0 ) ||
          ( iF < 0 && iS < 0 ) ||
          ( iF > 0 && iS < 0 && rF > 0 && rS > 0 ) ||
          ( iF < 0 && iS > 0 && rF > 0 && rS > 0 ) ) {
      dPhi = std::arg( coefA[i+1] ) - std::arg( coefA[i] );
    }
    if ( iF > 0 && iS < 0 && rF < 0 && rS < 0 )
      dPhi = std::arg( coefA[i+1] ) - std::arg( coefA[i] ) + 2*M_PI;
    if ( iF < 0 && iS > 0 && rF < 0 && rS < 0 )
      dPhi = std::arg( coefA[i+1] ) - std::arg( coefA[i] ) - 2*M_PI;

    phi += dPhi;
  }

  if ( error == -1 )
    return error;
  else
    return phi;
}

std::vector<double> Signal::nSolitonsCauchyParallel(std::vector<std::complex<double>> xi) {


  std::vector<std::complex<double>> a( xi.size() );

  // omp_set_num_threads(OMP_NUM_THREADS);
  omp_set_nested(1);

  unsigned int i;
  #pragma omp parallel private(i) shared(a)
  #pragma omp for schedule(runtime)
  for ( i = 0; i < xi.size(); i++ ) {
    // if( i == 0 )
    //   std::cout << "OMP THREADS NUMBER IS " << omp_get_num_threads() << std::endl;
    std::vector<std::complex<double>> coefficients(2);
    coefficients = this->calculateCoefficientsParallel( xi[i] );
    a[i] = coefficients[0];
  }

  double nSolitons = this->deltaArgParallel(a);
  std::vector<double> result(3);
  result[0] = 0.;
  result[1] = 0.;
  result[2] = 0.;


  if ( nSolitons == -1 ) {
    std::cerr << "Error in argument calculation" << std::endl;
    result[1] = -1;
  }
  nSolitons /= (2*M_PI);
  double eps = 0.2;

  //std::cout << nSolitons <<std::endl;

  result[0] = round(nSolitons);
  result[2] = nSolitons;
  if( result[1] != -1 ) {
    if ( abs( nSolitons - result[0] ) < eps )
      result[1] = 0;
    else
      result[1] = 1;
  }

  return result;
}

std::vector<double> Signal::getNSolitonsBox(std::string boxType, double boxSize, int N, std::complex<double> startPoint) {
  std::vector<std::complex<double>>* xi;
  if( boxType == "rect" )
    xi = getRectBoxXi(boxSize, N, startPoint);
  // if( boxType == "hex")
  //   std::vector<std::complex<double>> = getHexBoxXi(boxSize, N, startPoint);

  return this->nSolitonsCauchyParallel( *xi );
}

std::vector<solitonMap> Signal::getSolitonMap(std::string boxType, double boxSize, int N, std::complex<double> startPoint) {

  // Calculate general number of solitons for signal
  // It can take a lot of time
  double xiGridL[5] = {pow(2,7), pow(2,5), pow(2,4), pow(2,5), pow(2,7)};
  int xiGridN[5] = {(int)pow(2,10), (int)pow(2,11), (int)pow(2,11), (int)pow(2,11), (int)pow(2,10)};
  std::vector<double> nSoliton;
  int count = 0;
  do {
    if( count > 20 )
      throw std::logic_error("Can not find right grid to calculate number of solitons");

    if( count > 0 ) {
      for(int i = 0; i < 5; i++ )
        xiGridN[i] *= 2;
    }

    std::vector<std::complex<double>> xi = gridXi( 5, xiGridN, xiGridL );
    nSoliton = this->nSolitonsCauchyParallel( xi );

    count++;
  }
  while( nSoliton[1] != 0 );

  // Search for zones where we have only one complex eigenvalue
  // This zones will be write in solitonMap structure
  // std::vector<solitonMap>* map = new std::vector<solitonMap>((int)nSoliton[0]);
  std::vector<solitonMap> map((int)nSoliton[0]);
  for( int i = 0; i < nSoliton[0]; i++ ) {
    map[i].N = 0;
    map[i].boxSize = 0;
    map[i].startPoint = 0;
  }

  bool improveType = false;
  count = 0;
  do {
    if( count > 20 )
      throw std::logic_error("Can not find right box to calculate initial eigenvalues positions");

    if( count > 0 ) {
      if( improveType ) {
        boxSize *= 2;
        startPoint.real( startPoint.real()*2 );
      }
      else
        N *= 2;
        improveType = !improveType;
    }

    // TODO: check next calculations. If they are bad, we have to increase grid step
    map[0].N = getNSolitonsBox(boxType, boxSize, N, startPoint)[0];
    map[0].boxSize = boxSize;
    map[0].startPoint = startPoint;

    count++;
  }
  while( map[0].N != nSoliton[0] );

  bool check = false;
  count = 0;
  while( !check ) {

    // std::cout << "Step = " << count << std::endl;
    // for( int i = 0; i < nSoliton[0]; i++ )
      // std::cout << map[i].N << " " << map[i].boxSize << " " << map[i].startPoint << std::endl;

    if( count > 10000 )
      throw std::logic_error("Some eigenvalue was lost in calculations.");

    check = true;
    int sumN = 0;
    int startNum = 0;
    for( int i = 0; i < nSoliton[0]; i++ ) {
      if( map[i].N != 1 )
        check = false;

      if( startNum == 0 && map[i].N != 1 && map[i].N != 0 )
        startNum = i;

      sumN += map[i].N;
    }
    if( sumN != nSoliton[0] ) {
      // std::cout << "Step = " << count << std::endl;
      // for( int i = 0; i < nSoliton[0]; i++ )
        // std::cout << map[i].N << " " << map[i].boxSize << " " << map[i].startPoint << std::endl;
      throw std::logic_error("Sum of eigenvalues in zones is not equal to total number of solitons");
    }

    std::complex<double> initStartPoint = map[startNum].startPoint;
    std::complex<double> newStartPoint;
    double newBoxSize = map[startNum].boxSize/2.;
    int needN = map[startNum].N;
    int zoneSumN = 0;

    map[startNum].startPoint = 0;
    map[startNum].boxSize = 0;
    map[startNum].N = 0;

    int zoneNumber = 0;
    if( boxType == "rect" )
      zoneNumber = 4;
    for( int curZoneNum = 0; curZoneNum < zoneNumber; curZoneNum++ ) {
      switch (curZoneNum) {
        case 0: {
          newStartPoint.real(initStartPoint.real());
          newStartPoint.imag(initStartPoint.imag());
          break;
        }
        case 1: {
          newStartPoint.real(initStartPoint.real() + newBoxSize);
          newStartPoint.imag(initStartPoint.imag());
          break;
        }
        case 2: {
          newStartPoint.real(initStartPoint.real() + newBoxSize);
          newStartPoint.imag(initStartPoint.imag() + newBoxSize);
          break;
        }
        case 3: {
          newStartPoint.real(initStartPoint.real());
          newStartPoint.imag(initStartPoint.imag() + newBoxSize);
          break;
        }
      }

      int firstZero = 0;
      for( int i = 0; i < nSoliton[0]; i++ )
        if( map[i].N == 0 )
          firstZero = i;

      // TODO: check next calculations. If they are bad, we have to increase grid step
      int tempN = getNSolitonsBox(boxType, newBoxSize, N, newStartPoint)[0];
      double tempBoxSize = newBoxSize;
      std::complex<double> tempStartPoint = newStartPoint;

      // std::cout << "Zone number = " << curZoneNum << " tempN = " << tempN << std::endl;
      // std::cout << "Start Point = " << newStartPoint << std::endl;

      if( tempN != 0 ) {
        map[firstZero].N = tempN;
        map[firstZero].boxSize = tempBoxSize;
        map[firstZero].startPoint = tempStartPoint;
      }

      zoneSumN += tempN;
      if( zoneSumN == needN )
        break;
      if( zoneSumN > needN )
        throw std::logic_error("Number of eigenvalues in subzones more than in initial zone");
    }

    check = true;
    for( int i = 0; i < nSoliton[0]; i++ )
      if( map[i].N != 1 )
        check = false;

    count++;
  }

  return map;
}

std::vector<std::complex<double>> Signal::findSolitonsCouchy() {

  std::vector<solitonMap> map = this->getSolitonMap("rect", pow(2,6), pow(2,14), -pow(2,5) - 0.03);
  int nEigenvalues = map.size();

  std::vector<std::complex<double>> eigenvalues(nEigenvalues);
  for( int i = 0; i < nEigenvalues; i++ ) {
    std::complex<double> oneEigenvalue = this->findZeroBox("rect", map[i]);
    if( oneEigenvalue == -1.)
      throw std::logic_error("Can not find function zero in the box");
    eigenvalues[i] = oneEigenvalue;
  }

  return eigenvalues;
}

std::complex<double> Signal::findZeroBox(std::string boxType, solitonMap cell) {

  std::vector<std::complex<double>> result;
  if( boxType == "rect" ) {

    const int n = 20;
    solitonMap subCell {0, 0., 0.};

    for( int i = 0; i < n; i++ ) {
      for( int j = 0; j < n; j++ ) {
        double overcover = 1.01; // Add 1% to subsell size to cover borders
        if( i == n - 1 || j == n - 1 )
          overcover = 1.0; // If it is border subcell we do not need to cover
        subCell.boxSize = cell.boxSize / n * overcover;
        subCell.startPoint.real(cell.startPoint.real() + cell.boxSize / (double)n * (double)j );
        subCell.startPoint.imag(cell.startPoint.imag() + cell.boxSize / (double)n * (double)i );

        result = getZeroNewton(subCell);
        if( result[1] == 0. )
          return result[0];
      }
    }

  }

  return -1;
}

std::vector<std::complex<double>> Signal::getZeroNewton(solitonMap cell) {

  double EPS = 1E-16;
  std::complex<double> zprev( cell.startPoint.real() + cell.boxSize * 1. / 3., cell.startPoint.imag() + cell.boxSize * 1. / 3. );
  std::complex<double> znext( cell.startPoint.real() + cell.boxSize * 2. / 3., cell.startPoint.imag() + cell.boxSize * 2. / 3. );
  std::vector<std::complex<double>> fnext = {1};
  std::vector<std::complex<double>> fprev;

  int count = 0;
  // while( isInside(cell, znext) && std::abs(zprev - znext) > EPS ) {
  while( isInside(cell, znext) && std::abs(fnext[0]) > EPS ) {
    fnext = this->calculateCoefficientsParallel(znext);
    fprev = this->calculateCoefficientsParallel(zprev);

    std::complex<double> ztemp = znext;
    znext = znext - fnext[0]*(znext - zprev)/(fnext[0] - fprev[0]); // [0] is coefficent 'a' from function Signal::calculateCoefficientsParallel
    zprev = ztemp;

    if( count > 1000000 )
      throw std::logic_error("Out of time for Newton method");
    count++;
  }

  std::vector<std::complex<double>> result(2);
  if( isInside(cell, znext) ) {
    result[0] = znext;
    // std::cout << "f = " << fnext[0] << std::endl;
    result[1] = 0.;
  }
  else {
    result[0] = 0.;
    result[1] = -1.;
  }

  return result;
}


std::vector<std::complex<double>> Signal::fourierCollocation() {

  const int N = 4; // Number of modes
  const int matrixSize = 4*(N+1)*(N+1);
  std::vector<std::complex<double>> eigenvalues;
  double k0 = 2.0*M_PI/this->getTimeBoxSize();

  std::vector<std::complex<double>> signalFourier(N+1);
  for( int i = 0; i <= N; i++ ) {
    std::complex<double> sum;
    std::complex<double> imUnit(0, 1);
    for( int k = 0; k < this->nSignal; k++ ) {
      sum += 1.0 / this->nSignal * this->signal[k] *
        exp(-imUnit * k0 * (double)(i - N/2) *
            ( -this->timeBoxSize/2.0 + this->timeBoxSize / this->nSignal * k )
        );
    }

    signalFourier[i] = sum;
  }

  // for( int i = 0; i < signalFourier.size(); i++ )
  //   std::cout << signalFourier[i] << " ";
  // std::cout << std::endl << std::endl;

  std::complex<double> collocationMatrix[matrixSize];
  // Initialising matrix
  for( int i = 0; i < 4*(N+1)*(N+1); i++ )
    collocationMatrix[i] = 0.0;

  // Insert diagonal elements
  for( int i = 0; i < 2*(N+1); i++ ) {
    if( i < N+1 ) {
      collocationMatrix[i + i*2*(N+1)] = N/2 - i;
    }
    else {
      collocationMatrix[i + i*2*(N+1)] = -N/2 + i - (N + 1);
    }
  }


  for( int k = 0; k < 2*(N+1); k++ ) {
    for( int i = 0; i < 2*(N+1); i++ ) {
      if( ( i >= N + 1 ) && ( k < N + 1 ) ) {
        // Right top submatrix
        int iLoc = i - (N + 1);
        int kLoc = k;
        int index = kLoc - iLoc + N/2;
        // std::cout << iLoc << " " << kLoc << " " << index << std::endl;
        if( index >= 0 && index < N + 1 )
          collocationMatrix[i + k*2*(N + 1)] = signalFourier[index];
      }
      if( ( i < N + 1 ) && ( k >= N + 1 ) ) {
        // Left bottom submatrix
        int iLoc = i;
        int kLoc = k - (N + 1);
        int index = iLoc - kLoc + N/2;
        if( index >= 0 && index < N + 1 )
          collocationMatrix[i + k*2*(N + 1)] = std::conj(signalFourier[index]);
        // std::cout << iLoc << " " << kLoc << " " << index << std::endl;
      }
    }
  }

  // for( int k = 0; k < 2*(N+1); k++ ) {
  //   for( int i = 0; i < 2*(N+1); i++ ) {
  //     std::cout << collocationMatrix[i + k*2*(N + 1)] << " ";
  //   }
  //   std::cout << std::endl;
  // }

  // MKL_Complex16 matrix[matrixSize];
  // for( int k = 0; k < 2*(N+1); k++ ) {
  //   for( int i = 0; i < 2*(N+1); i++ ) {
  //     matrix[i + k*2*(N + 1)].real = std::real(collocationMatrix[i + k*2*(N + 1)]);
  //     matrix[i + k*2*(N + 1)].imag = std::imag(collocationMatrix[i + k*2*(N + 1)]);
  //   }
  // }

  // lapack_complex_double* matrix = new lapack_complex_double[matrixSize];
  const MKL_INT nMatrix = matrixSize;
  MKL_Complex16 matrix[nMatrix];
  for( int k = 0; k < 2*(N + 1); k++ ) {
    for( int i = 0; i < 2*(N + 1); i++ ) {
      matrix[i + k*2*(N + 1)].real = std::real(collocationMatrix[i + k*2*(N + 1)]);
      matrix[i + k*2*(N + 1)].imag = std::imag(collocationMatrix[i + k*2*(N + 1)]);
    }
  }

  // Next part is mkl functions for complex matrix
  // zgebal -> zgehrd -> zhseqr
  const MKL_INT lda = 2*(N + 1);

  MKL_INT ilo, ihi;
  double scale;
  MKL_INT zgebalStatus = LAPACKE_zgebal( LAPACK_ROW_MAJOR, 'B', nMatrix, matrix, lda, &ilo, &ihi, &scale );
  // std::complex<double> tau[2*(N+1)];
  MKL_Complex16 tau[lda];
  MKL_INT zgehrdStatus = LAPACKE_zgehrd( LAPACK_ROW_MAJOR, nMatrix, ilo, ihi, matrix, 2*(N+1), tau);

  MKL_Complex16 eig[lda];
  MKL_INT zhseqrStatus = LAPACKE_zhseqr( LAPACK_ROW_MAJOR, 'E', 'N', nMatrix, ilo, ihi, matrix, lda, eig, NULL, 1 );

  std::cout << zgebalStatus << std::endl;
  std::cout << zgehrdStatus << std::endl;
  std::cout << zhseqrStatus << std::endl;

  // for( int k = 0; k < 2*(N+1); k++ ) {
  //   for( int i = 0; i < 2*(N+1); i++ ) {
  //     std::cout << collocationMatrix[i + k*2*(N + 1)] << " ";
  //   }
  //   std::cout << std::endl;
  // }

  for( int k = 0; k < lda; k++ )
    std::cout << "(" << eig[k].real << "," << eig[k].imag << ") ";

  std::cout << std::endl;

  return eigenvalues;
}
