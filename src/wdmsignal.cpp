#ifndef WDM_SIGNAL_CPP
#define WDM_SIGNAL_CPP

#include "wdmsignal.h"

double sinc( double t ) {
  if( t == 0 )
    return 1;
  else
    return sin(t)/t;
}

double raisedCosine( double t, double T, double beta = 0.1 ) {
  if( std::abs(t) == T/(2.*beta) ) {
    return M_PI/(4.*T)*sinc(1./(2.*beta));
  }
  else {
    return 1./T*sinc(t/T)*(cos(M_PI*beta*t/T))/(1 - pow(2*beta*t/T, 2));
  }
}

// double carrierFun( double t, double T ) {
//   return 1;
// }

// double carrierFun( double t, double T ) {
//   return 1./2.*(1 - cos( 2*M_PI*t/T ));
// }

double carrierFun( double t, double T ) {

  double mod = t - T*(int)(t/T);
  if( mod <= T/4. || mod >= 3./4.*T ) {
    return 1./2.*(1 - cos( 4*M_PI*t/T ));
  }
  else {
    return 1.;
  }
  return 0;
}

// double carrierFun( double t, double T ) {
//
//   return raisedCosine(t, T);
// }

WdmSignal::WdmSignal() : Signal() {
  modType = "empty";
  nSub = 0;
}

WdmSignal::WdmSignal( int nSignal, double timeBoxSize, double dw, std::string normType, double normValue,
  std::string modType, int nSub) : Signal() {


  this->setTimeBoxSize(timeBoxSize);
  this->setSignalType("wdm");
  this->setSignal( WdmSignal::getWdm( nSignal, dw, modType, nSub ) );
  if ( normType == "l1" || normType == "L1" ) {
    this->setNormL1(normValue);
    this->setNormType("l1");
  }
  if ( normType == "l2" || normType == "L2" ) {
    this->setNormL2(normValue);
    this->setNormType("l2");
  }
  this->rescaleSignal();
  this->modType = modType;
  this->nSub = nSub;
}

void WdmSignal::printParameters() {
  Signal::printParameters();
  std::cout << "\n***** WDM PARAMETERS *****\n" << std::endl
    << "WDM modulation type : " << modType << std::endl
    << "Number of subcarriers : " << nSub << std::endl;
}

int WdmSignal::printToFile(std::string file, std::ios_base::openmode mode ) {
  std::ofstream outFile;
  outFile.open(file, mode);
  outFile << this->getSignalType() << std::endl
    << this->getNormType() << std::endl
    << this->getNormL1() << std::endl
    << this->getNormL2() << std::endl
    << this->getPeakToAverage() << std::endl
    << this->getTimeBoxSize() << std::endl
    << modType << std::endl
    << nSub << std::endl
    << this->getNData() << std::endl;

  std::vector<std::complex<double>> data = getData();
  std::vector<std::complex<double>> signal = getSignal();
  for ( int i = 0; i < this->getNData(); i++ ) {
    outFile << data[i].real() << " " << data[i].imag() << std::endl;
  }
  outFile << this->getNSignal() << std::endl;
  for ( int i = 0; i < this->getNSignal(); i++ ) {
    outFile << signal[i].real() << " " << signal[i].imag() << std::endl;
  }
  outFile.close();

  return 0;
}

std::vector<std::complex<double>> WdmSignal::getRandomConstellation(std::string modType, int size, int randomIn) {

  if ( modType != "qpsk" && modType != "16qam" && modType != "64qam" && modType != "1024qam" ) {
    std::cout << "********************************" << std::endl;
    std::cout << "WDM error: no such modulation type" << std::endl;
    std::cout << "********************************" << std::endl;
  }

  std::mt19937 gen(time(0) + randomIn);
  std::discrete_distribution<> discrQPSK({0, 1}); // probability to generate 0 = 0%, 1 = 100%
  std::discrete_distribution<> discr16QAM({0, 1, 0, 1}); // probability 0 = 0%, 1 = 50%, 2 = 0%, 3 = 50%
  std::discrete_distribution<> discr64QAM({0, 1, 0, 1, 0, 1, 0, 1});
  std::discrete_distribution<> discr1024QAM(
    {0, 1, 0, 1, 0, 1, 0, 1,
      0, 1, 0, 1, 0, 1, 0, 1,
      0, 1, 0, 1, 0, 1, 0, 1,
      0, 1, 0, 1, 0, 1, 0, 1});

  std::discrete_distribution<> signD({1,1});

  std::vector<std::complex<double>> result;
  for ( int i = 0; i < size; i++ ) {
    // I use 2 signs to move between quarters
    // +- | ++
    // -------
    // -- | -+
    int signOne;
    int signTwo;

    if ( signD(gen) == 0 )
      signOne = -1;
    else
      signOne = 1;

    if ( signD(gen) == 0 )
      signTwo = -1;
    else
      signTwo = 1;

    double realPart;
    double imagPart;
    if( modType == "qpsk" ) {
      realPart = discrQPSK(gen);
      imagPart = discrQPSK(gen);
    }
    if( modType == "16qam" ) {
      realPart = discr16QAM(gen);
      imagPart = discr16QAM(gen);
    }
    if( modType == "64qam" ) {
      realPart = discr64QAM(gen);
      imagPart = discr64QAM(gen);
    }
    if( modType == "1024qam" ) {
      realPart = discr1024QAM(gen);
      imagPart = discr1024QAM(gen);
    }

    std::complex<double> complexNumber(realPart*signOne,imagPart*signTwo);
    result.push_back(complexNumber);
  }

  return result;
}

std::vector<std::complex<double>> WdmSignal::getWdm( int nSignal, double dw,
  std::string modType, int nSub) {

  // double dw = 3.;
  double dt = this->getTimeBoxSize() / (double)nSignal;
  const std::complex<double> j(0, 1);

  std::vector<std::complex<double>> signal(nSignal);
  for( int n = -(nSub - (nSub % 2))/2; n <= (nSub - (nSub % 2))/2; n++ ) {
    // std::cout << n << std::endl;
    std::complex<double> qam = WdmSignal::getRandomConstellation( modType, 1, n )[0];
    // std::cout << "Data = " << qam << std::endl;
    for( int i = 0; i < nSignal; i++ ) {
      double t = -this->getTimeBoxSize()/2. + i*dt;
      signal[i] += qam * std::exp( -j*2.*M_PI*dw*(double)n*t ) * carrierFun(t + this->getTimeBoxSize()/2., this->getTimeBoxSize());
      // signal[i] += qam * std::exp( -j*2.*M_PI*dw*(double)n*t ) * carrierFun(t + this->getTimeBoxSize()/2., 1.);
    }
  }

  return signal;
}

#endif
