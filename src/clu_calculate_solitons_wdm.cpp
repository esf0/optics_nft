#include "signal.h"
#include "ofdmsignal.h"
#include "satsumayajimasignal.h"
#include "wdmsignal.h"
#include "nftanalyse.h"
#include <chrono>

int main(int argc, char** argv) {

  int nSamples = 1;
  // double timeRange = 1.;
  std::string normType("l1");
  double normValue = 1.;
  //int nData = (int)pow(2, 10);
  std::string modType("qpsk");
  int nSub = 128;
  double dw = 3.;
  //int nFFT = 1024;
  //double prefix = 1./128;

  if ( argc <= 1 ) {
    // std::cout << "Error: require input arguments\n";
    std::cerr << "Error: require input arguments\n";
    return -1;
  }
  else {
    nSamples = atoi( argv[1] );
    normType = argv[2];
    normValue = atof( argv[3] );
    modType = argv[4];
    nSub = atoi( argv[5] );
    dw = atof( argv[6] );
    std::cout << "nSamples = " << nSamples << std::endl;
    std::cout << "normType = " << normType << std::endl;
    std::cout << "normValue = " << normValue << std::endl;
    std::cout << "modType = " << modType << std::endl;
    std::cout << "nSub = " << nSub << std::endl;
    std::cout << "dw = " << dw << std::endl;
  }

  // Create vector xi to calculate N solitons
  // Xi is a grid in spectral space on real axe

  int N[3] = { (int)pow( 2, 13 ), (int)pow( 2, 14 ), (int)pow( 2, 13 ) };
  double L[3] = { pow( 2, 7 ), pow( 2, 4 ), pow( 2, 7 ) };
  // int N[3] = {8, 8, 8};
  // double L[3] = {2., 1., 2.};
  std::vector<std::complex<double>> xi = gridXi( 3, N, L );
  std::vector<double> out;
  std::vector<int> nSolitons;
  std::vector<int> err;

  std::string infoFileName("info.txt");
  std::ofstream resultFile("result.txt", std::ios::out | std::ios::app );
  if( resultFile.is_open() ) {
    // std::cout << "File opened\n";
    for ( int i = 0; i < nSamples; i++ ) {
      auto chBegin = std::chrono::high_resolution_clock::now();

      WdmSignal wdmSignal( (int)pow(2, 10), 1., dw, normType, normValue, modType, nSub );
      wdmSignal.addLateral(0, pow(2, 9));
      wdmSignal.printToFile( infoFileName, std::ios::out | std::ios::app );
      out = wdmSignal.nSolitonsCauchyParallel(xi);
      nSolitons.push_back( out[0] );
      err.push_back( out[1] );

      auto chEnd = std::chrono::high_resolution_clock::now();
      resultFile << out[0] << " " << out[1] << " " << out[2] << " " <<
                    std::chrono::duration_cast<std::chrono::milliseconds>(chEnd-chBegin).count() <<
                    std::endl;
    }
    resultFile.close();
    // std::cout << "File closed\n";
  }

  return 0;
}
