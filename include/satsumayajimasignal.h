// This file define class SatsumaYajimaSignal
// author: Egor Sedov
// date: 24.07.2018
// e-mail: egor.sedoff@gmail.com

#ifndef SATSUMAYAJIMASIGNAL_H
#define SATSUMAYAJIMASIGNAL_H 1

#include "signal.h"

class SatsumaYajimaSignal : public Signal {
private:
  double ampl;

public:
  SatsumaYajimaSignal();
  SatsumaYajimaSignal(double, double, int);
  void printParameters();
  int printToFile(std::string);

};

#endif
