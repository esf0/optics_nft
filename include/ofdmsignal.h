// This file define subclass OfdmSignal
// author: Egor Sedov
// date: 29.06.2018
// e-mail: egor.sedoff@gmail.com

#ifndef OFDM_SIGNAL_H
#define OFDM_SIGNAL_H

#include "signal.h"

class OfdmSignal : public Signal {
private:
  std::string modType; //Modulation type
  int nSub; //Number of subcarriers
  int nFFT; //Number of Fourier modes
  double nPrefix; //Cycle prefix

public:
  OfdmSignal();
  OfdmSignal(double, std::string, double, int, std::string, int, int, double);
  void printParameters();
  int printToFile(std::string file, std::ios_base::openmode mode = std::ios_base::out);

  std::vector<std::complex<double>> getRandomConstellation(std::string, int, int);
  std::vector<std::complex<double>> getOfdm(int, std::string, int, int, double);
};

#endif
