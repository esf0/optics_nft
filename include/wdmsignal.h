// This file define subclass WdmSignal
// author: Egor Sedov
// date: 03.03.2018
// e-mail: egor.sedoff@gmail.com

#ifndef WDM_SIGNAL_H
#define WDM_SIGNAL_H

#include "signal.h"

class WdmSignal : public Signal {
private:
  std::string modType; //Modulation type
  int nSub; //Number of subcarriers

public:
  WdmSignal();
  WdmSignal( int nSignal, double timeBoxSize, double dw, std::string normType, double normValue,
    std::string modType, int nSub);

  void printParameters();
  int printToFile(std::string file, std::ios_base::openmode mode = std::ios_base::out);

  std::vector<std::complex<double>> getRandomConstellation(std::string, int, int);
  std::vector<std::complex<double>> getWdm( int, double, std::string, int);

};

#endif
