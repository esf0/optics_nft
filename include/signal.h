// This file define class Signal
// author: Egor Sedov
// date: 29.06.2018
// e-mail: egor.sedoff@gmail.com

#ifndef SIGNAL_H
#define SIGNAL_H

#define _USE_MATH_DEFINES
#include <math.h>

#include <complex>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <random>
#include <ctime>
#include <stdio.h>
// #include <stdlib.h>
#include <omp.h>

// #define MKL_Complex16 std::complex<double> // have to define before mkl.h
#include <mkl.h>
#include <stdexcept>

#include "Array.h"
#include "fftw++.h"
#include "nftanalyse.h"

#ifndef EPSILON
#define EPSILON 1e-16
#endif

// #ifndef OMP_NUM_THREADS
// #define OMP_NUM_THREADS 4
// #endif
//
// #ifndef OMP_NESTED
// #define OMP_NESTED 1
// #endif
//
// #ifndef OMP_SCHEDULE
// #define OMP_SCHEDULE "static, 5"
// #endif

#ifdef WIN32
  #define GNUPLOT_NAME "pguplot -persist"
#else
  #define GNUPLOT_NAME "gnuplot -persist"
#endif



/*
  Class Signal describe complex solution of Nonlinear Shrodinger Equation.
  Methods of this class make Direct and Inverse Nonlinear Fourier Transformation
  and also calculate Nonlinear Spectrum and Scattering Coefficients
*/

class Signal {
private:
  std::string signalType; //Signal type
  std::vector<std::complex<double>> signal; //Complex signal
  int nSignal; //Number of points in signal
  std::vector<std::complex<double>> data; //Complex input data, which was encoded
  int nData; //Number of input data
  double normL1; //Value of L1 norm
  double normL2; //Value of L2 norm
  double peakToAverage; //Peak to average ratio
  std::string normType;

  double timeBoxSize; //Time interval


  std::vector<std::complex<double>> spectrum; //Nonlinear spectrum

  std::vector<std::complex<double>> coefXi; //Value of xi for which next coefficients are calculated
  std::vector<std::complex<double>> coefA; //Scattering coefficient a
  std::vector<std::complex<double>> coefB; //Scattering coefficient b
  std::vector<std::complex<double>> derivA; //Derivative of scattering coefficient a: da/dxi
  std::vector<std::complex<double>> derivB; //Derivative of scattering coefficient b: db/dxi

  std::vector<std::complex<double>> coefC; //Scattering coefficient C = i*r for discrete spectrum
  std::vector<std::complex<double>> coefR; //Scattering coefficient r

  std::vector<std::complex<double>> kernel; //Kernel

public:
  Signal();
  Signal(std::vector<std::complex<double>>& signal, double timeBoxSize);
  //Signal(int, char*, int, int, double);
  //Signal(const Signal&);
  //~Signal();

  void setSignalType(std::string);
  void setSignal(std::vector<std::complex<double>>);
  void setData(std::vector<std::complex<double>>);
  void setNData(int);
  void setNormL1(double); //This function only set value of norm
  // void setNewNormL1(double l1); //This function set new norm value and recalculate signal
  // void setNewNormL2(double l2); //This function set new norm value and recalculate signal
  void setNormL2(double); //This function only set value of norm
  void setNormType(std::string); //Set initial type of the norm
  void setTimeBoxSize(double);
  void setNewTimeBoxSize(double timeBox); // Set new time box size and rescale signal

  std::vector<std::complex<double>> getSignal();
  std::vector<std::complex<double>> getData();
  std::string getSignalType();
  int getNSignal();
  int getNData();
  double getNormL1();
  double getNormL1(bool); //If bool == 1 recalculate l1 norm for signal
  double getNormL2();
  double getNormL2(bool); //If bool == 1 recalculate l1 norm for signal
  double getPeakToAverage();
  std::string getNormType();
  double getTimeBoxSize();
  std::vector<std::complex<double>> getCoefXi();
  std::vector<std::complex<double>> getCoefA();

  double calculatePeakToAverage(); //Calculate and set peakToAverage ratio
  int rescaleSignal(); //Use this to rescale signal with value of 'normType' norm
  int addLateral(int type, int nPoints); //Add lateral intervals for calculations

  void printParameters(); //Print parameters of the signal
  int printSignalToFile(std::string file, std::ios_base::openmode mode = std::ios_base::out); // Print only time and signal to file
  virtual int printToFile(std::string file, std::ios_base::openmode mode = std::ios_base::out);

  void drawSignal();


  int directTIB();
  int inverseTIB();

  std::vector<std::complex<double>> calculateCoefficients(std::complex<double> xi); //Return vector with coefficients a and b
  double deltaArg(std::vector<std::complex<double>>);
  std::vector<double> nSolitonsCauchy(std::vector<std::complex<double>>);

  // TODO: combine function with and without parallel calculations
  std::vector<std::complex<double>> calculateCoefficientsParallel(std::complex<double> xi);
  double deltaArgParallel(std::vector<std::complex<double>>);
  std::vector<double> nSolitonsCauchyParallel(std::vector<std::complex<double>>);

  // Next block of functions devote to spetrum calculations
  std::vector<double> getNSolitonsBox(std::string boxType, double boxSize, int N, std::complex<double> startPoint);
  std::vector<solitonMap> getSolitonMap(std::string boxType, double boxSize, int N, std::complex<double> startPoint);
  std::vector<std::complex<double>> findSolitonsCouchy();
  std::complex<double> findZeroBox(std::string boxType, solitonMap cell);
  std::vector<std::complex<double>> getZeroNewton(solitonMap cell);

  std::vector<std::complex<double>> fourierCollocation();

protected:
};

#endif
