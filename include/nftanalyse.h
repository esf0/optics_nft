// This file define NFT analyse methods
// author: Egor Sedov
// date: 29.06.2018
// e-mail: egor.sedoff@gmail.com

#ifndef NFTANALYSE_H
#define NFTANALYSE_H

#include <stdio.h>
#include <complex>
#include <vector>

struct solitonMap {
  int N;
  double boxSize;
  std::complex<double> startPoint;
};

std::vector<std::complex<double>> gridXi( int n, int* N, double* L);
std::vector<std::complex<double>>* getRectBoxXi(double boxSize, int N, std::complex<double> startPoint);
std::vector<std::complex<double>>* getHexBoxXi(double boxSize, int N, std::complex<double> startPoint);
std::complex<double> findZeroBox(std::string boxType, solitonMap cell, std::vector<std::complex<double>> (*f)(std::complex<double>));
std::vector<std::complex<double>> getZeroNewton(solitonMap cell, std::vector<std::complex<double>> (*f)(std::complex<double>));
bool isBorder(solitonMap cell, std::complex<double> point);
bool isInside(solitonMap cell, std::complex<double> point);

#endif
